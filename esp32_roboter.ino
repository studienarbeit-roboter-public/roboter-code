
#include <TinyGPS++.h>
#include "BluetoothSerial.h"
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled!
#endif

HardwareSerial uart(1);
TinyGPSPlus gps;
BluetoothSerial SerialBT;

// Motor Pins
const int motor1vor = 2;
const int motor1ruck = 0;
const int motor2vor = 4;
const int motor2ruck = 5;

// GPS Daten
double lat;
double longitude;
int month;
int day;
int year;
int hour;
int minute;
int second;
unsigned short sats;
unsigned long course;
unsigned long arduinoTimestamp;

// PWM Einstellungen
const int freq = 4000;
const int resolution = 8;

// Variablen zur Erfassung der empfangenen Geschwindkeitsangabe
char lastChar = 'u'; // letzter empfangener Fahrbefehl (vorwaerts oder ruckwaerts)
int lastCharNums = 0; // Anzahl der empfangen Geschwindkeitsziffern
int lastCharNumbers[3]; // Speicher für Geschwindigkeit

void setup() {
  // uart begin für GPS
  uart.begin(4800, SERIAL_8N1, 16, 17);
  //Serial begin für Testzwecke (seriellen Monitor der IDE nutzen)
  Serial.begin(115200);
  // Bluetooth begin
  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("Roboter gestartet. Die Bluetooth-Kopplung ist jetzt moeglich!");

  // Motor Pins setup
  ledcSetup(motor1vor, freq, resolution);
  ledcSetup(motor2vor, freq, resolution);
  ledcSetup(motor1ruck, freq, resolution);
  ledcSetup(motor2ruck, freq, resolution);
  ledcAttachPin(motor1vor, motor1vor);
  ledcAttachPin(motor2vor, motor2vor);
  ledcAttachPin(motor1ruck, motor1ruck);
  ledcAttachPin(motor2ruck, motor2ruck);
  
  arduinoTimestamp = millis();
}

void loop() {
  // GPS Daten erfassen
  while (uart.available() > 0)
  {
    Serial.flush();
    if (gps.encode(uart.read()))
    {
      lat = gps.location.lat();
      longitude = gps.location.lng();
      month = gps.date.month();
      day = gps.date.day();
      year = gps.date.year();
      hour = gps.time.hour();
      minute = gps.time.minute();
      second = gps.time.second();
      sats = gps.satellites.value();
      course = gps.course.deg();
      //displayInfo();
    }
  }

  // Zum Testen der Bluetooth-Verbindung
  /*
  if (Serial.available()) {
    SerialBT.write(Serial.read());
  }
  */

  // Bluetooth abhoeren
  if (SerialBT.available()) {
    // Zum Testen
    /*
    char c = (char)SerialBT.read();
    Serial.print(c);
    */

    // Fahrbefehl Vorwaerts empfangen - Warte auf Geschwindigkeit
    if (c == 'v')
    {
      lastCharNums = 0;
    }

    // Fahrbefehl Rueckwaerts empfangen - Warte auf Geschwindigkeit
    if (c == 'z')
    {
      lastCharNums = 0;
    }

    // Fahrbefehl links empfangen
    if (c == 'l')
    {
      links();
    }

    // Fahrbefehl rechts empfangen
    if (c == 'r')
    {
      rechts();
    }

    // Fahrbefehl stopp empfangen
    if (c == 's')
    {
      resetMotor();
    }

    // Positionsanforderung empfangen
    if (c == 'p')
    {
      SerialBT.print("S");
      SerialBT.print(lat, 6);
      SerialBT.print(", ");
      SerialBT.print(longitude, 6);
      SerialBT.print(", ");
      SerialBT.print(course);
      SerialBT.print("°, ");
      SerialBT.print(day);
      SerialBT.print(".");
      SerialBT.print(month);
      SerialBT.print(".");
      SerialBT.print(year);
      SerialBT.print(", ");
      SerialBT.print(hour);
      SerialBT.print(":");
      SerialBT.print(minute);
      SerialBT.print(":");
      SerialBT.print(second);
      SerialBT.print(", sats: ");
      SerialBT.print(sats);
      SerialBT.print("E");
      arduinoTimestamp = millis();
    }

    // Geschwindkeit empfangen
    if (checkCharIsNumber(c) && lastChar != 'u')
    {
      if (lastCharNums < 3)
      {
        lastCharNumbers[lastCharNums] = (int) c;
        lastCharNums++;
      }
      if (lastCharNums >= 3)
      {
        lastCharNums = 0;
        int speed = ((lastCharNumbers[0] - 48) * 100) + ((lastCharNumbers[1] - 48) * 10) + (lastCharNumbers[2] - 48);

        if (lastChar == 'v' && speed <= 255)
        {
          geradeVor(speed);
        }
        else if (lastChar == 'z' && speed <= 255)
        {
          geradeRuck(speed);
        }
        lastChar = 'u';
      }
    }
    else
    {
      lastChar = c;
    }
    
  }
  checkVerbunden();
}


void geradeVor()
{
  resetMotor();
  ledcWrite(motor1vor, 255);
  ledcWrite(motor2vor, 255);
}

void geradeVor(int speed)
{
  resetMotor();
  ledcWrite(motor1vor, speed);
  ledcWrite(motor2vor, speed);
}

void geradeRuck()
{
  resetMotor();
  ledcWrite(motor1ruck, 255);
  ledcWrite(motor2ruck, 255);
}

void geradeRuck(int speed)
{
  resetMotor();
  ledcWrite(motor1ruck, speed);
  ledcWrite(motor2ruck, speed);
}

void resetMotor()
{
  ledcWrite(motor1ruck, 0);
  ledcWrite(motor2ruck, 0);
  ledcWrite(motor1vor, 0);
  ledcWrite(motor2vor, 0);
}

void kurzeKurve()
{
  resetMotor();
  delay(100);
  resetMotor();
}

void rechts()
{
  resetMotor();
  ledcWrite(motor1vor, 255);
  ledcWrite(motor2ruck, 255);
  delay(20);
  resetMotor();
}

void links()
{
  resetMotor();
  ledcWrite(motor1ruck, 255);
  ledcWrite(motor2vor, 255);
  delay(20);
  resetMotor();
}

void checkVerbunden()
{
  unsigned long tmpTimestamp = millis();
  if ((tmpTimestamp - arduinoTimestamp) > 3000)
  {
    resetMotor();
  }
}

void displayInfo()
{
  Serial.print(F("Location: ")); 
  if (true) //gps.location.isValid())
  {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
    Serial.print(F(" Course: "));
    Serial.print(gps.course.deg());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid())
  {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F("  SATS: "));
  Serial.print(gps.satellites.value());
 

  Serial.println();
}

int checkCharIsNumber(char c)
{
  return c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9';
}
